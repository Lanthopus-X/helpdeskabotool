﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelpdeskAboTool.Models;
using HelpdeskAboTool.Controllers;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Data;
using Xunit;

namespace Tests.Controller.EstimateItem
{
    
    public abstract class EstimateItemControllerTest
    {
        #region Seeding
        protected DbContextOptions<UrenMenuContext> ContextOptions { get; }
        protected EstimateItemControllerTest(DbContextOptions<UrenMenuContext> contextOptions)
        {
            ContextOptions = contextOptions;

            Seed();
        }

        private void Seed()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var Inloggen = new List<HelpdeskAboTool.Models.EstimateItem>
                {
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Inloggen",
                        Description = "Het laten inloggen van een gebruiker",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Registreren",
                        Description = "Een gebruiker een acount laten aanmaken",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Gebruiker beheer",
                        Description = "Gebruikers aanmaken, wijzigen en verwijderen",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Rollen",
                        Description = "Gebruikers met verschillenden rollen",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Rollen Beheer",
                        Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                        CostEstimate = 4
                    }

                };
                

                context.AddRange(Inloggen);

                context.SaveChanges();
            }
        }
        #endregion

        #region CanGetItems
        [Fact]
        public async void Can_get_items()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem();

                var items = result.Value.ToList();

                Assert.Equal(5, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);
            }
        }

        [Fact]
        public async void Can_get_items_by_Id()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(2);

                var items = result.Value;

                Assert.Equal("Registreren", items.Name);

            }
        }

        [Fact]
        public async void Can_get_items_by_Id_composit()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(false);

                var items = result.Value.ToList();

                var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = items,
                    CostEstimate = 8M
                };

                var added = context.Add(compositEstimateItem);
                var saved = context.SaveChanges();
            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);
                var result = await controller.GetEstimateItem(6);
                var items = result.Value;

                Assert.Equal("Rollen CRUD", items.Name);
                Assert.Equal(5, items.EstimateItems.Count());
            }
        }

        [Fact]
        public async void Can_get_items_by_Id_composit_level2()
        {
            int id = 0;
            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(false);

                var items = result.Value.ToList();

                var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = items,
                    CostEstimate = 8M
                };

                var compositEstimateItem2 = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD 2",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = new List<HelpdeskAboTool.Models.EstimateItem>{compositEstimateItem},
                    CostEstimate = 8M
                };

                var added = context.Add(compositEstimateItem);
                var added2 = context.Add(compositEstimateItem2);
                var saved = context.SaveChanges();
                id = added2.Entity.Id;
            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);
                var result = await controller.GetEstimateItem(id, true,3);
                var items = result.Value;

                Assert.Equal("Rollen CRUD 2", items.Name);
                Assert.Single(items.EstimateItems);
                Assert.Equal(5, items.EstimateItems[0].EstimateItems.Count());
            }
        }

        [Fact]
        public async void Can_get_hours()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem();

                var items = result.Value.ToList();

                Assert.Equal(5, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal(2M, items[0].CostEstimate);
                Assert.Equal("Rollen Beheer", items[4].Name);
                Assert.Equal(4M, items[4].CostEstimate);
            }

        }

        [Fact]
        public async void Can_get_hours_composit()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(false);

                var items = result.Value.ToList();

                var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = items
                };

                var added = context.Add(compositEstimateItem);
                var saved = context.SaveChanges();
            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);
                var result = await controller.GetEstimateItem();
                var items = result.Value.ToList();


                Assert.Equal(6, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal(12M, items[5].CostEstimate);
            }


        }

        [Fact]
        public async void Can_get_hours_only_from_children()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(false);

                var items = result.Value.ToList();

                var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = items,
                    CostEstimate = 8M
                };

                var added = context.Add(compositEstimateItem);
                var saved = context.SaveChanges();

                result = await controller.GetEstimateItem();
                items = result.Value.ToList();


                Assert.Equal(6, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal(12M, items[5].CostEstimate);
            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);
                var hours = controller.GetEstimateItemHours(6).Result.Value;
                Assert.Equal(12M, hours);
            }


        }

        [Fact]
        public async void Can_get_hours_only_from_children_by_Id()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem(false);

                var items = result.Value.ToList();

                var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Rollen CRUD",
                    Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                    EstimateItems = items,
                    CostEstimate = 8M
                };

                var added = context.Add(compositEstimateItem);
                var saved = context.SaveChanges();
            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {

                var controller = new EstimateItemsController(context);
                var result = await controller.GetEstimateItem(6);
                var items = result.Value;

                Assert.Equal(12M, items.CostEstimate);
            }
        }

        #endregion

        #region CanInsertItem

        [Fact]
        public async void Can_Insert_Items()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem();

                var items = result.Value.ToList();

                Assert.Equal(5, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);


            }

            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var estimateItem = new HelpdeskAboTool.Models.EstimateItem()
                {
                    Name = "Test",
                    Description = "Test",
                    CostEstimate = 2.5M,

                };

                _ = await controller.PostEstimateItem(estimateItem);

                var result = await controller.GetEstimateItem();

                var items = result.Value.ToList();

                Assert.Equal(6, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);

                Assert.Equal("Test", items[5].Name);
                Assert.Equal("Test", items[5].Description);
                Assert.Equal(2.5M, items[5].CostEstimate);
            }


        }
        #endregion

        #region CanEditItems

        [Fact]
        public async void Can_edit_Items()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var result = await controller.GetEstimateItem();

                var items = result.Value.ToList();

                Assert.Equal(5, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);

                var customer = items[0];
                customer.Name = "Jan";
                _ = await controller.PutEstimateItem(customer.Id, customer);

                Assert.Equal(5, items.Count);
                Assert.Equal("Jan", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);
            }
        }

        #endregion

        #region CanDeleteItems

        [Fact]
        public async void Can_Delete_Items()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimateItemsController(context);

                var items = controller.GetEstimateItem().Result.Value.ToList();

                Assert.Equal(5, items.Count);
                Assert.Equal("Inloggen", items[0].Name);
                Assert.Equal("Registreren", items[1].Name);
                Assert.Equal("Gebruiker beheer", items[2].Name);
                Assert.Equal("Rollen", items[3].Name);
                Assert.Equal("Rollen Beheer", items[4].Name);

                var toDelete = items[0];

                _ = controller.DeleteEstimateItem(toDelete.Id);

                items = controller.GetEstimateItem().Result.Value.ToList();

                Assert.Equal(4, items.Count);
                Assert.Equal("Registreren", items[0].Name);
                Assert.Equal("Gebruiker beheer", items[1].Name);
                Assert.Equal("Rollen", items[2].Name);
                Assert.Equal("Rollen Beheer", items[3].Name);
            }
        }

        #endregion
    }
}
