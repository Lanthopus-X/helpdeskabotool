﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Data;
using HelpdeskAboTool.Data.Service;
using HelpdeskAboTool.Models;
using HelpdeskAboTool.Models.CLR;

namespace HelpdeskAboTool.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstimatesController : ControllerBase
    {
        #region Constructor
       
        private readonly UrenMenuContext _context;

        public EstimatesController(UrenMenuContext context)
        {
            _context = context;
        }
        
        #endregion

        #region Get

        // GET: api/Estimates
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Estimate>>> GetEstimate(bool useProjection = true, int maxDepth = 10)
        {
            if (useProjection)
                return await _context.Estimate.OrderByDescending(e => e.Id).Select(EstimateService.GetEstimateProjection(maxDepth)).ToListAsync();
            var result = await _context.Estimate.Include(i => i.EstimateItemsList).Include(i => i.Customer).ToListAsync();
            return result;
        }

        // GET: api/Estimates/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Estimate>> GetEstimate(int id, bool useProjection = true, int maxDepth = 10)
        {
            Estimate estimate;
            if (useProjection)
                estimate = await _context.Estimate.Select(EstimateService.GetEstimateProjection(maxDepth)).Where(e => e.Id == id).FirstOrDefaultAsync();
            else 
                estimate = await _context.Estimate.Include(e => e.EstimateItemsList).Include( e => e.Customer).Where(e => e.Id == id).FirstOrDefaultAsync();

            if (estimate == null)
            {
                return NotFound();
            }

            return estimate;
        }

        // GET: api/Estimates/Hour/5
        [HttpGet("Hour/{id}")]
        public async Task<ActionResult<decimal>> GetEstimaTotalHours(int id, bool useProjection = true, int maxDepth = 10)
        {
            Estimate estimate;
            if (useProjection)
                estimate = await _context.Estimate.Select(EstimateService.GetEstimateProjection(maxDepth)).Where(e => e.Id == id).FirstOrDefaultAsync();
            else
                estimate = await _context.Estimate.Include(e => e.EstimateItemsList).Include(e => e.Customer).Where(e => e.Id == id).FirstOrDefaultAsync();

            if (estimate == null)
            {
                return NotFound();
            }

            return estimate.TotalCost;
        }


        #endregion

        #region Put

        // PUT: api/Estimates/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstimate(int id, Estimate estimate)
        {
            if (id != estimate.Id)
            {
                return BadRequest();
            }

            _context.Entry(estimate).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstimateExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        #endregion

        #region Post

        // POST: api/Estimates
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Estimate>> PostEstimate([FromForm] Estimate estimate)
        {
            var customer = await _context.Customer.FirstAsync(Customer => Customer.Id == estimate.Customer.Id);
            estimate.Customer = customer;
            _context.Estimate.Add(estimate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstimate", new { id = estimate.Id }, estimate);
        }

        // POST: api/Estimates
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("Add/{id}/{itemId}")]
        public async Task<ActionResult<Estimate>> AddEstimateItems(int Id, int itemId)
        {
            var estimate = _context.Estimate
                .Include(e => e.EstimateItemsList)
                .FirstOrDefault(e => e.Id == Id);
            var estimateItem = await _context.EstimateItem.FindAsync(itemId);
            estimate.EstimateItemsList.Add(estimateItem);
            if (estimate.EstimateItems == null)
                estimate.EstimateItems = new List<EstimateEstimateItem>();
            estimate.EstimateItems.Add(new EstimateEstimateItem()
            {
                EstimateItem = estimateItem,
                Estimate = estimate,
                EstimateId = estimate.Id,
                EstimateItemId = estimateItem.Id
            });
            _context.Update(estimate);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstimate", new { id = Id });
        }

        #endregion

        #region Delete

        // DELETE: api/Estimates/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEstimate(int id)
        {
            var estimate = await _context.Estimate.FindAsync(id);
            if (estimate == null)
            {
                return NotFound();
            }

            _context.Estimate.Remove(estimate);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        [HttpDelete("removeItem/{itemId}/{estimateId}")]
        public async Task<ActionResult<Estimate>> RemoveItem(int itemId, int estimateId)
        {
            var estimate = await _context.Estimate.Include(ei => ei.EstimateItemsList).FirstOrDefaultAsync(e => e.Id == estimateId);
            if (estimate == null)
            {
                return NotFound();
            }

            estimate.EstimateItemsList.Remove(await _context.EstimateItem.FindAsync(itemId));
            _context.Estimate.Update(estimate);
            await _context.SaveChangesAsync();

            return estimate;
        }

        #endregion

        #region Private

        private bool EstimateExists(int id)
        {
            return _context.Estimate.Any(e => e.Id == id);
        }

        #endregion

    }
}
