﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Data;
using HelpdeskAboTool.Data.Service;
using HelpdeskAboTool.Models;

namespace HelpdeskAboTool.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstimateItemsController : ControllerBase
    {
        #region Constructor

        private readonly UrenMenuContext _context;

        public EstimateItemsController(UrenMenuContext context)
        {
            _context = context;
        }
        

        #endregion

        #region Get

        // GET: api/EstimateItems
        [HttpGet]
        public async Task<ActionResult<IEnumerable<EstimateItem>>> GetEstimateItem(bool useProjection = true, int maxDepth = 10, CustomerType customerType = CustomerType.ZZP)
        {
            if(useProjection) 
                return await _context.EstimateItem.Where(estimateItem => estimateItem.CustomerType == customerType).OrderByDescending(estimateItem => estimateItem.ListPriority).Select(EstimateService.GetEstimateItemProjection(maxDepth)).ToListAsync();
            return await _context.EstimateItem.ToListAsync();
        }

        // GET: api/EstimateItems/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EstimateItem>> GetEstimateItem(int id, bool useProjection = true, int maxDepth = 10)
        {
            EstimateItem estimateItem;
            if(useProjection)
                estimateItem = await _context.EstimateItem.Where(i => i.Id == id).Select(EstimateService.GetEstimateItemProjection(maxDepth)).FirstOrDefaultAsync();
            else
                estimateItem = await _context.EstimateItem.Where(i => i.Id == id).FirstOrDefaultAsync();
            if (estimateItem == null)
            {
                return NotFound();
            }

            return estimateItem;
        }

        // GET: api/EstimateItems/Hours/5
        [HttpGet("Hours/{id}")]
        public async Task<ActionResult<decimal>> GetEstimateItemHours(int id,int maxDepth = 10)
        {
            var estimateItem = await _context.EstimateItem.Where(i=>i.Id == id).Select(EstimateService.GetEstimateItemProjection(maxDepth)).FirstOrDefaultAsync();

            if (estimateItem == null)
            {
                return NotFound();
            }

            return estimateItem.CostEstimate;
        }

        #endregion

        #region Put

        // PUT: api/EstimateItems/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutEstimateItem(int id, EstimateItem estimateItem)
        {
            if (id != estimateItem.Id)
            {
                return BadRequest();
            }

            _context.Entry(estimateItem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EstimateItemExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        #endregion

        #region Post

        // POST: api/EstimateItems
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<EstimateItem>> PostEstimateItem([FromForm]EstimateItem estimateItem)
        {
            _context.EstimateItem.Add(estimateItem);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetEstimateItem", new { id = estimateItem.Id }, estimateItem);
        }

        #endregion

        #region Delete

        // DELETE: api/EstimateItems/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEstimateItem(int id)
        {
            var estimateItem = await _context.EstimateItem.FindAsync(id);
            if (estimateItem == null)
            {
                return NotFound();
            }

            _context.EstimateItem.Remove(estimateItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        #endregion

        #region Private

        private bool EstimateItemExists(int id)
        {
            return _context.EstimateItem.Any(e => e.Id == id);
        }

        #endregion
    }
}
