﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class manytomanyestimateItemestimateItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstimateItem_Estimate_EstimateId",
                table: "EstimateItem");

            migrationBuilder.DropForeignKey(
                name: "FK_EstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateItem");

            migrationBuilder.DropIndex(
                name: "IX_EstimateItem_EstimateId",
                table: "EstimateItem");

            migrationBuilder.DropIndex(
                name: "IX_EstimateItem_EstimateItemId",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "EstimateId",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "EstimateItemId",
                table: "EstimateItem");

            migrationBuilder.CreateTable(
                name: "EstimateEstimateItem",
                columns: table => new
                {
                    BelongsToEstimatesId = table.Column<int>(type: "int", nullable: false),
                    EstimateItemsListId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstimateEstimateItem", x => new { x.BelongsToEstimatesId, x.EstimateItemsListId });
                    table.ForeignKey(
                        name: "FK_EstimateEstimateItem_Estimate_BelongsToEstimatesId",
                        column: x => x.BelongsToEstimatesId,
                        principalTable: "Estimate",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EstimateEstimateItem_EstimateItem_EstimateItemsListId",
                        column: x => x.EstimateItemsListId,
                        principalTable: "EstimateItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "EstimateItemEstimateItem",
                columns: table => new
                {
                    BelongToEstimateItemsId = table.Column<int>(type: "int", nullable: false),
                    EstimateItemsId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EstimateItemEstimateItem", x => new { x.BelongToEstimateItemsId, x.EstimateItemsId });
                    table.ForeignKey(
                        name: "FK_EstimateItemEstimateItem_EstimateItem_BelongToEstimateItemsId",
                        column: x => x.BelongToEstimateItemsId,
                        principalTable: "EstimateItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EstimateItemEstimateItem_EstimateItem_EstimateItemsId",
                        column: x => x.EstimateItemsId,
                        principalTable: "EstimateItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_EstimateEstimateItem_EstimateItemsListId",
                table: "EstimateEstimateItem",
                column: "EstimateItemsListId");

            migrationBuilder.CreateIndex(
                name: "IX_EstimateItemEstimateItem_EstimateItemsId",
                table: "EstimateItemEstimateItem",
                column: "EstimateItemsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EstimateEstimateItem");

            migrationBuilder.DropTable(
                name: "EstimateItemEstimateItem");

            migrationBuilder.AddColumn<int>(
                name: "EstimateId",
                table: "EstimateItem",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EstimateItemId",
                table: "EstimateItem",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EstimateItem_EstimateId",
                table: "EstimateItem",
                column: "EstimateId");

            migrationBuilder.CreateIndex(
                name: "IX_EstimateItem_EstimateItemId",
                table: "EstimateItem",
                column: "EstimateItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateItem_Estimate_EstimateId",
                table: "EstimateItem",
                column: "EstimateId",
                principalTable: "Estimate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateItem",
                column: "EstimateItemId",
                principalTable: "EstimateItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
