import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './css/index.css'
import './index.css'
import './tailwind.css'
import './css/site.css'
import './css/tailwind.css'

createApp(App).use(router).mount('#app')
