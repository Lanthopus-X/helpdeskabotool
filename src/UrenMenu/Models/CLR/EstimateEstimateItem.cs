﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HelpdeskAboTool.Models.CLR
{
    public class EstimateEstimateItem
    {
        public int Id { get; set; }
        public int EstimateId { get; set; }
        public Estimate Estimate { get; set; }
        public int EstimateItemId { get; set; }
        public EstimateItem EstimateItem { get; set; }

    }
}
