﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using HelpdeskAboTool.Models;

namespace HelpdeskAboTool.Data.Service
{
    public class EstimateService
    {
        public static Expression<Func<EstimateItem, EstimateItem>> GetEstimateItemProjection(int maxDepth = 0, int currentDepth = 0)
        {
            currentDepth++;

            Expression<Func<EstimateItem, EstimateItem>> result = estimateItem => new EstimateItem()
            {
                Id = estimateItem.Id,
                Name = estimateItem.Name,
                CostEstimate = estimateItem.CostEstimate,
                Description = estimateItem.Description,
                EstimateItems = currentDepth >= maxDepth
                    ? new List<EstimateItem>()
                    : estimateItem.EstimateItems.AsQueryable().Select(GetEstimateItemProjection(maxDepth, currentDepth))
                        .ToList()

            };

            return result;
        }

        public static Expression<Func<Estimate, Estimate>> GetEstimateProjection(int maxDepth = 0, int currentDepth = 0)
        {
            currentDepth++;

            Expression<Func<Estimate, Estimate>> result = estimateItem => new Estimate()
            {
                Id = estimateItem.Id,
                Customer = estimateItem.Customer,
                EstimateItemsList = currentDepth == maxDepth
                    ? new List<EstimateItem>()
                    : estimateItem.EstimateItemsList.AsQueryable().Select(GetEstimateItemProjection(maxDepth, currentDepth))
                        .ToList()

            };

            return result;
        }
    }
}