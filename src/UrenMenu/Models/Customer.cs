﻿namespace HelpdeskAboTool.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ZipCode { get; set; }
        public string City { get; set; }
        public string Email { get; set; }
        public CustomerType CustomerType { get; set; }

    }

    public enum CustomerType
    {
        ZZP, MKB, Concern
    }
}