﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelpdeskAboTool.Models;
using HelpdeskAboTool.Controllers;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Data;
using Xunit;

namespace Tests.Controller.Customer
{
    
    public abstract class CustomerControllerTest
    {
        #region Seeding
        protected DbContextOptions<UrenMenuContext> ContextOptions { get; }
        protected CustomerControllerTest(DbContextOptions<UrenMenuContext> contextOptions)
        {
            ContextOptions = contextOptions;

            Seed();
        }

        private void Seed()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var one = new HelpdeskAboTool.Models.Customer
                {
                    Name = "Stefan",
                    Email = "info@test.nl"
                };

                context.AddRange(one);

                context.SaveChanges();
            }
        }
        #endregion

        #region CanGetCustomers
        [Fact]
        public async void Can_get_Customers()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new CustomersController(context);

                var result = await controller.GetCustomer();

                var items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Name);
            }
        }
        #endregion

        #region CanInsertCustomers

        [Fact]
        public async void Can_Insert_Customers()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new CustomersController(context);

                var result = await controller.GetCustomer();

                var items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Name);

               
            }

            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new CustomersController(context);

                var newCustomer = new HelpdeskAboTool.Models.Customer
                {
                    Name = "Luuk",
                    Email = "Fake@Fake.nl",
                    City = "Eindhoven",
                    PhoneNumber = "0681099232",
                    ZipCode = "5617BK"

                };

                _ = await controller.PostCustomer(newCustomer);

                var result = await controller.GetCustomer();

                var items = result.Value.ToList();

                Assert.Equal(2, items.Count);
                Assert.Equal("Stefan", items[0].Name);
                Assert.Equal("Luuk", items[1].Name);
            }


        }
        #endregion

        #region CanEditCustomer

        [Fact]
        public async void Can_edit_Customer()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new CustomersController(context);

                var result = await controller.GetCustomer();

                var items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Name);

                var customer = items[0];
                customer.Name = "Jan";
                _ = await controller.PutCustomer(customer.Id, customer);

                Assert.Single(items);
                Assert.Equal("Jan", items[0].Name);
            }
        }

        #endregion

        #region CanDeleteCustomer

        [Fact]
        public async void Can_Delete_Customer()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new CustomersController(context);

                var items = controller.GetCustomer().Result.Value.ToList();

                Assert.Single(items);

                var toDelete = items[0];

                _ = controller.DeleteCustomer(toDelete.Id);

                items = controller.GetCustomer().Result.Value.ToList();

                Assert.Empty(items);
            }
        }

        #endregion


    }
}
