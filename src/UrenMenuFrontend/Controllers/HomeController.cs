﻿using HelpdeskAboToolFrontend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using UrenMenu.Models;

namespace HelpdeskAboToolFrontend.Controllers
{
    public class HomeController : Controller
    {
        private static readonly string basePath = "https://localhost:44332";
        private static readonly HttpClient client = new HttpClient();
        private static Estimate _estimate;
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public async Task<IActionResult> _Estimate(int id = 1)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(basePath + "/api/Estimates/" + id + "?useProjection=true&maxDepth=4");
                _estimate = JsonConvert.DeserializeObject<Estimate>(await response.Content.ReadAsStringAsync());
                ViewData["Estimate"] = _estimate;
            }
            catch
            {
            }

            return PartialView();
        }

        public async Task<IActionResult> _EstimateItemList()
        {
            return PartialView();
        }

        public async Task<IActionResult> CreateEstimateItem()
        {
            try
            {
                int customerId = 4;
                HttpResponseMessage response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
                ViewData["EstimateItems"] = new List<EstimateItem>();
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public async Task<IActionResult> Estimate(int id = 1)
        {
            try
            {
                HttpResponseMessage response = await client.GetAsync(basePath + "/api/Estimates/" + id + "?useProjection=true&maxDepth=4");
                _estimate = JsonConvert.DeserializeObject<Estimate>(await response.Content.ReadAsStringAsync());
                ViewData["Estimate"] = _estimate;
                int customerId = 0;
                if (_estimate?.Customer?.CustomerType != null)
                {
                    customerId = _estimate.Customer.CustomerType;
                }
                response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
            }

            return View();
        }

        public async Task<IActionResult> EstimateItems()
        {
            try
            {
                int customerId = 4;
                HttpResponseMessage response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
                ViewData["EstimateItems"] = new List<EstimateItem>();
            }
            try
            {
                HttpResponseMessage response = await client.GetAsync(basePath + "/api/EstimateItems?useProjection=true&maxDepth=4&customerType=0");
                List<EstimateItem> estimateItemList = JsonConvert.DeserializeObject<List<EstimateItem>>(await response.Content.ReadAsStringAsync());
                ViewData["ZZP"] = estimateItemList;
                response = await client.GetAsync(basePath + "/api/EstimateItems?useProjection=true&maxDepth=4&customerType=1");
                estimateItemList = JsonConvert.DeserializeObject<List<EstimateItem>>(await response.Content.ReadAsStringAsync());
                ViewData["MKB"] = estimateItemList;
                response = await client.GetAsync(basePath + "/api/EstimateItems?useProjection=true&maxDepth=4&customerType=2");
                estimateItemList = JsonConvert.DeserializeObject<List<EstimateItem>>(await response.Content.ReadAsStringAsync());
                ViewData["Concern"] = estimateItemList;
            }
            catch
            {
                ViewData["ZZP"] = new List<Estimate>();
                ViewData["MKB"] = new List<Estimate>();
                ViewData["Concern"] = new List<Estimate>();
            }
            ViewData["Id"] = 0;
            return View();
        }

        public async Task<IActionResult> Estimates()
        {
            try
            {
                int customerId = 4;
                HttpResponseMessage response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
                ViewData["EstimateItems"] = new List<EstimateItem>();
            }
            try
            {
                HttpResponseMessage response = await client.GetAsync(basePath + "/api/Estimates?useProjection=true&maxDepth=4");
                List<Estimate> estimateList = JsonConvert.DeserializeObject<List<Estimate>>(await response.Content.ReadAsStringAsync());
                ViewData["Estimates"] = estimateList;
            }
            catch
            {
                ViewData["Estimates"] = new List<Estimate>();
            }
            return View();
        }

        public IActionResult Index()
        {
            try
            {
                int customerId = 4;
                HttpResponseMessage response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
                ViewData["EstimateItems"] = new List<EstimateItem>();
            }
            return View();
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                int customerId = 0;
                if (_estimate?.Customer?.CustomerType != null)
                {
                    customerId = _estimate.Customer.CustomerType;
                }
                HttpResponseMessage response = client.GetAsync(basePath + "/api/EstimateItems?customerType=" + customerId + "&useProjection=true&maxDepth=4")?.Result;
                var estimateItems = JsonConvert.DeserializeObject<ICollection<EstimateItem>>(response.Content.ReadAsStringAsync().Result);
                ViewData["EstimateItems"] = estimateItems ?? new List<EstimateItem>();
            }
            catch
            {
                ViewData["EstimateItems"] = new List<EstimateItem>();
            }
        }
    }
}