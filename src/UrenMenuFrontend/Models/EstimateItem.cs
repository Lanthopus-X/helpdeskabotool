﻿using System.Collections.Generic;

namespace UrenMenu.Models
{
    public class EstimateItem
    {
        private decimal _costEstimate;
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int CustomerType { get; set; }
        public int ListPriority { get; set; } = 0;
        public decimal CostEstimate
        {
            get
            {
                decimal total = 0;

                if (EstimateItems == null)
                    return _costEstimate;
                if (EstimateItems.Count == 0)
                    return _costEstimate;

                foreach (var estimateItem in EstimateItems)
                {
                    total += estimateItem.CostEstimate;
                }

                return total;
            }
            set => _costEstimate = value;
        }

        public List<EstimateItem> EstimateItems { get; set; }

        public List<EstimateItem> BelongToEstimateItems { get; set; }

        public List<Estimate> BelongsToEstimates { get; set; }

    }
}