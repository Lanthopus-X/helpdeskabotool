﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class added_customer_type : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CustomerType",
                table: "EstimateItem",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CustomerType",
                table: "Customer",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerType",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "CustomerType",
                table: "Customer");
        }
    }
}
