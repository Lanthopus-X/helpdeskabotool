﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class IDtest2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstimateEstimateItem_Estimate_BelongsToEstimatesId",
                table: "EstimateEstimateItem");

            migrationBuilder.DropForeignKey(
                name: "FK_EstimateEstimateItem_EstimateItem_EstimateItemsListId",
                table: "EstimateEstimateItem");

            migrationBuilder.RenameColumn(
                name: "EstimateItemsListId",
                table: "EstimateEstimateItem",
                newName: "EstimateItemId");

            migrationBuilder.RenameColumn(
                name: "BelongsToEstimatesId",
                table: "EstimateEstimateItem",
                newName: "EstimateId");

            migrationBuilder.RenameIndex(
                name: "IX_EstimateEstimateItem_EstimateItemsListId",
                table: "EstimateEstimateItem",
                newName: "IX_EstimateEstimateItem_EstimateItemId");

            migrationBuilder.RenameIndex(
                name: "IX_EstimateEstimateItem_BelongsToEstimatesId",
                table: "EstimateEstimateItem",
                newName: "IX_EstimateEstimateItem_EstimateId");

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateEstimateItem_Estimate_EstimateId",
                table: "EstimateEstimateItem",
                column: "EstimateId",
                principalTable: "Estimate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateEstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateEstimateItem",
                column: "EstimateItemId",
                principalTable: "EstimateItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstimateEstimateItem_Estimate_EstimateId",
                table: "EstimateEstimateItem");

            migrationBuilder.DropForeignKey(
                name: "FK_EstimateEstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateEstimateItem");

            migrationBuilder.RenameColumn(
                name: "EstimateItemId",
                table: "EstimateEstimateItem",
                newName: "EstimateItemsListId");

            migrationBuilder.RenameColumn(
                name: "EstimateId",
                table: "EstimateEstimateItem",
                newName: "BelongsToEstimatesId");

            migrationBuilder.RenameIndex(
                name: "IX_EstimateEstimateItem_EstimateItemId",
                table: "EstimateEstimateItem",
                newName: "IX_EstimateEstimateItem_EstimateItemsListId");

            migrationBuilder.RenameIndex(
                name: "IX_EstimateEstimateItem_EstimateId",
                table: "EstimateEstimateItem",
                newName: "IX_EstimateEstimateItem_BelongsToEstimatesId");

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateEstimateItem_Estimate_BelongsToEstimatesId",
                table: "EstimateEstimateItem",
                column: "BelongsToEstimatesId",
                principalTable: "Estimate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateEstimateItem_EstimateItem_EstimateItemsListId",
                table: "EstimateEstimateItem",
                column: "EstimateItemsListId",
                principalTable: "EstimateItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
