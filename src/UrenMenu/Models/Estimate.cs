﻿using System;
using System.Collections.Generic;
using System.Linq;
using HelpdeskAboTool.Models.CLR;

namespace HelpdeskAboTool.Models
{
    public class Estimate
    {
        public int Id { get; set; }
        public Customer Customer { get; set; }
        public List<EstimateItem> EstimateItemsList { get; set; }
        public List<EstimateEstimateItem> EstimateItems { get; set; }
        public decimal TotalCost
        {
            get
            {
                decimal total = 0M;
                if(EstimateItemsList != null)
                    foreach (var estimateItem in EstimateItemsList)
                    {
                        total += estimateItem.CostEstimate;
                    }

                return total;
            }
        }
    }

    public enum EstimateStatus
    {
        Open, Closed, Approved, Denied
    }
}