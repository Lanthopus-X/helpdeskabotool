﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class selfrefranceestimateitem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "EstimateItemId",
                table: "EstimateItem",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EstimateItem_EstimateItemId",
                table: "EstimateItem",
                column: "EstimateItemId");

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateItem",
                column: "EstimateItemId",
                principalTable: "EstimateItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstimateItem_EstimateItem_EstimateItemId",
                table: "EstimateItem");

            migrationBuilder.DropIndex(
                name: "IX_EstimateItem_EstimateItemId",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "EstimateItemId",
                table: "EstimateItem");
        }
    }
}
