import { createRouter, createWebHistory } from 'vue-router'

const BasicLayout = () => import('../views/layouts/BasicLayout.vue')

const routes = [
    {
      path: '/',
      name: 'Index',
      component: BasicLayout
    }
  ]
  
  const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
  })
  
  export default router
  