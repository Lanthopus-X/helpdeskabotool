﻿using System;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using HelpdeskAboTool.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Tests.Controller.Customer;
using HelpdeskAboTool.Data;

namespace Tests.Controller.Estimate
{
    #region SqliteEstimateControllerTest
    public class SqliteEstimateControllerTest : EstimateControllerTest, IDisposable
    {
        private readonly DbConnection _connection;
        public SqliteEstimateControllerTest()
            : base(
                new DbContextOptionsBuilder<UrenMenuContext>()
                    .UseSqlite(CreateInMemoryDatabase())
                    .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void Dispose() => _connection.Dispose();
    }
    #endregion
}
