﻿using System;
using System.Data.Common;
using Microsoft.Data.Sqlite;
using HelpdeskAboTool.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Tests.Controller.Customer;
using Tests.Controller.Estimate;
using HelpdeskAboTool.Data;

namespace Tests.Controller.EstimateItem
{
    #region SqliteEstimateItemControllerTest
    public class SqliteEstimateItemControllerTest : EstimateItemControllerTest, IDisposable 
    {
        private readonly DbConnection _connection;
        public SqliteEstimateItemControllerTest()
            : base(
                new DbContextOptionsBuilder<UrenMenuContext>()
                    .UseSqlite(CreateInMemoryDatabase())
                    .Options)
        {
            _connection = RelationalOptionsExtension.Extract(ContextOptions).Connection;
        }

        private static DbConnection CreateInMemoryDatabase()
        {
            var connection = new SqliteConnection("Filename=:memory:");

            connection.Open();

            return connection;
        }

        public void Dispose() => _connection.Dispose();
    }
    #endregion
}
