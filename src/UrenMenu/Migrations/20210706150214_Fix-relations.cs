﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class Fixrelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "EstimateItem",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "EstimateId",
                table: "EstimateItem",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_EstimateItem_EstimateId",
                table: "EstimateItem",
                column: "EstimateId");

            migrationBuilder.AddForeignKey(
                name: "FK_EstimateItem_Estimate_EstimateId",
                table: "EstimateItem",
                column: "EstimateId",
                principalTable: "Estimate",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EstimateItem_Estimate_EstimateId",
                table: "EstimateItem");

            migrationBuilder.DropIndex(
                name: "IX_EstimateItem_EstimateId",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "EstimateItem");

            migrationBuilder.DropColumn(
                name: "EstimateId",
                table: "EstimateItem");
        }
    }
}
