﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HelpdeskAboTool.Models;
using HelpdeskAboTool.Controllers;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Data;
using Xunit;

namespace Tests.Controller.Estimate
{
    
    public abstract class EstimateControllerTest
    {
        #region Seeding
        protected DbContextOptions<UrenMenuContext> ContextOptions { get; }
        protected EstimateControllerTest(DbContextOptions<UrenMenuContext> contextOptions)
        {
            ContextOptions = contextOptions;

            Seed();
        }

        private void Seed()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var one = new HelpdeskAboTool.Models.Customer
                {
                    Name = "Stefan",
                    Email = "info@test.nl"
                };

                var Inloggen = new List<HelpdeskAboTool.Models.EstimateItem>
                {
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Inloggen",
                        Description = "Het laten inloggen van een gebruiker",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Registreren",
                        Description = "Een gebruiker een acount laten aanmaken",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Gebruiker beheer",
                        Description = "Gebruikers aanmaken, wijzigen en verwijderen",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Rollen",
                        Description = "Gebruikers met verschillenden rollen",
                        CostEstimate = 2
                    },
                    new HelpdeskAboTool.Models.EstimateItem()
                    {
                        Name = "Rollen Beheer",
                        Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                        CostEstimate = 4
                    }

                };
                var two = new HelpdeskAboTool.Models.Estimate()
                {
                    Customer = one,
                    EstimateItemsList = Inloggen

                };

                context.AddRange(Inloggen);
                context.Add(one);
                context.Add(two);

                context.SaveChanges();
            }
        }
        #endregion

        #region CanGetEstimate
        [Fact]
        public async void Can_get_Estimate()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimatesController(context);

                var result = await controller.GetEstimate();

                var items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Customer.Name);
            }
        }
        #endregion

        #region CanEditEstimate

        [Fact]
        public async void Can_edit_Estimate()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimatesController(context);

                var result = await controller.GetEstimate();

                var items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Customer.Name);

                var toEdit = items[0];

                toEdit.Customer.Name = "Luuk";

                _ = controller.PutEstimate(toEdit.Id, toEdit);

                result = await controller.GetEstimate();
                items = result.Value.ToList();

                Assert.Single(items);
                Assert.Equal("Luuk", items[0].Customer.Name);
            }
        }

        #endregion

        #region CanInsertEstimate

        [Fact]
        public async void Can_insert_Estimate()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var estimatesController = new EstimatesController(context);
                var customersController = new CustomersController(context);
                var estimateItemsController = new EstimateItemsController(context);

                var newEstimate = new HelpdeskAboTool.Models.Estimate()
                {
                    Customer = customersController.GetCustomer().Result.Value.ToList()[0],
                    EstimateItemsList = estimateItemsController.GetEstimateItem(false).Result.Value.ToList()
                };
                var result = await estimatesController.GetEstimate();

                var items = result.Value.ToList();
                Assert.Single(items);
                Assert.Equal("Stefan", items[0].Customer.Name);

                _ = await estimatesController.PostEstimate(newEstimate);
                result = await estimatesController.GetEstimate();

                items = result.Value.ToList();
                Assert.Equal(2,items.Count);
                Assert.Equal("Stefan", items[0].Customer.Name);
                Assert.Equal("Stefan", items[1].Customer.Name);

                Assert.Equal(5, items[1].EstimateItemsList.Count);
                Assert.Equal("Inloggen", items[1].EstimateItemsList[0].Name);
                Assert.Equal("Registreren", items[1].EstimateItemsList[1].Name);
                Assert.Equal("Gebruiker beheer", items[1].EstimateItemsList[2].Name);
                Assert.Equal("Rollen", items[1].EstimateItemsList[3].Name);
                Assert.Equal("Rollen Beheer", items[1].EstimateItemsList[4].Name);


            }

        }

        #endregion

        #region CanDeleteEstimate

        [Fact]
        public async void Can_delete_Estimate()
        {
            using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimatesController(context);
                var items = controller.GetEstimate().Result.Value.ToList();

                Assert.Single(items);

                var toDelete = items[0];
                _ = controller.DeleteEstimate(toDelete.Id);
                items = controller.GetEstimate().Result.Value.ToList();

                Assert.Empty(items);
            }
        }

        #endregion

        #region CanGetTotalHours

        [Fact]
        public async void Can_get_total_hours()
        {
            await using (var context = new UrenMenuContext(ContextOptions))
            {
                var controller = new EstimatesController(context);

                var totalHours = await controller.GetEstimaTotalHours(1);

                Assert.Equal(12M, totalHours.Value);
            }
        }

        #endregion
    }
}
