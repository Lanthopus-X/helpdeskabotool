﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using HelpdeskAboTool.Data;

namespace HelpdeskAboTool.Migrations
{
    [DbContext(typeof(UrenMenuContext))]
    [Migration("20210712134129_many-to-many-estimateItem-estimateItem")]
    partial class manytomanyestimateItemestimateItem
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EstimateEstimateItem", b =>
                {
                    b.Property<int>("BelongsToEstimatesId")
                        .HasColumnType("int");

                    b.Property<int>("EstimateItemsListId")
                        .HasColumnType("int");

                    b.HasKey("BelongsToEstimatesId", "EstimateItemsListId");

                    b.HasIndex("EstimateItemsListId");

                    b.ToTable("EstimateEstimateItem");
                });

            modelBuilder.Entity("EstimateItemEstimateItem", b =>
                {
                    b.Property<int>("BelongToEstimateItemsId")
                        .HasColumnType("int");

                    b.Property<int>("EstimateItemsId")
                        .HasColumnType("int");

                    b.HasKey("BelongToEstimateItemsId", "EstimateItemsId");

                    b.HasIndex("EstimateItemsId");

                    b.ToTable("EstimateItemEstimateItem");
                });

            modelBuilder.Entity("UrenMenu.Models.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("City")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("PhoneNumber")
                        .HasColumnType("int");

                    b.Property<string>("ZipCode")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Customer");
                });

            modelBuilder.Entity("UrenMenu.Models.Estimate", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("CustomerId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Estimate");
                });

            modelBuilder.Entity("UrenMenu.Models.EstimateItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Description")
                        .HasColumnType("nvarchar(max)");

                    b.Property<decimal>("HourEstimate")
                        .HasColumnType("decimal(18,2)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("EstimateItem");
                });

            modelBuilder.Entity("EstimateEstimateItem", b =>
                {
                    b.HasOne("UrenMenu.Models.Estimate", null)
                        .WithMany()
                        .HasForeignKey("BelongsToEstimatesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("UrenMenu.Models.EstimateItem", null)
                        .WithMany()
                        .HasForeignKey("EstimateItemsListId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("EstimateItemEstimateItem", b =>
                {
                    b.HasOne("UrenMenu.Models.EstimateItem", null)
                        .WithMany()
                        .HasForeignKey("BelongToEstimateItemsId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("UrenMenu.Models.EstimateItem", null)
                        .WithMany()
                        .HasForeignKey("EstimateItemsId")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();
                });

            modelBuilder.Entity("UrenMenu.Models.Estimate", b =>
                {
                    b.HasOne("UrenMenu.Models.Customer", "Customer")
                        .WithMany()
                        .HasForeignKey("CustomerId");

                    b.Navigation("Customer");
                });
#pragma warning restore 612, 618
        }
    }
}
