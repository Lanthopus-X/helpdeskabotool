﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class addedlistpriority : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ListPriority",
                table: "EstimateItem",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ListPriority",
                table: "EstimateItem");
        }
    }
}
