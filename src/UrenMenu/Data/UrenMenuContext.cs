﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HelpdeskAboTool.Models;
using HelpdeskAboTool.Models.CLR;

namespace HelpdeskAboTool.Data
{
    public class UrenMenuContext : DbContext
    {
        public UrenMenuContext (DbContextOptions<UrenMenuContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<Estimate>()
                .HasMany(e => e.EstimateItemsList)
                .WithMany(e => e.BelongsToEstimates)
                .UsingEntity<EstimateEstimateItem>
                (
                    j => j
                        .HasOne(eei => eei.EstimateItem)
                        .WithMany(e => e.EstimateItemsList)
                        .HasForeignKey( eei => eei.EstimateItemId),
                    j => j
                        .HasOne(eei => eei.Estimate)
                        .WithMany(e=> e.EstimateItems)
                        .HasForeignKey(eei => eei.EstimateId)
                );
        }

        private void Seed()
        {
            var one = new Customer
            {
                Name = "Stefan",
                Email = "info@test.nl"
            };

            var inloggen = new List<EstimateItem>
                {
                    new EstimateItem()
                    {
                        Name = "Inloggen",
                        Description = "Het laten inloggen van een gebruiker",
                        CostEstimate = 2
                    },
                    new EstimateItem()
                    {
                        Name = "Registreren",
                        Description = "Een gebruiker een acount laten aanmaken",
                        CostEstimate = 2
                    },
                    new EstimateItem()
                    {
                        Name = "Gebruiker beheer",
                        Description = "Gebruikers aanmaken, wijzigen en verwijderen",
                        CostEstimate = 2
                    },
                    new EstimateItem()
                    {
                        Name = "Rollen",
                        Description = "Gebruikers met verschillenden rollen",
                        CostEstimate = 2
                    },
                    new EstimateItem()
                    {
                        Name = "Rollen Beheer",
                        Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                        CostEstimate = 4
                    }

                };

            var two = new HelpdeskAboTool.Models.Estimate()
            {
                Customer = one,
                EstimateItemsList = inloggen

            };
            var three = new HelpdeskAboTool.Models.Estimate()
            {
                Customer = one,
                EstimateItemsList = inloggen

            };

            var compositEstimateItem = new HelpdeskAboTool.Models.EstimateItem()
            {
                Name = "Rollen CRUD",
                Description = "Het kunnen aanmaken, wijzigen en verwijderen van rollen",
                EstimateItems = inloggen,
                CostEstimate = 8M
            };

            AddRange(inloggen);
            Add(compositEstimateItem);
            Add(one);
            AddRange(two, three);

            SaveChanges();
        }


        public DbSet<Customer> Customer { get; set; }

        public DbSet<Estimate> Estimate { get; set; }

        public DbSet<EstimateItem> EstimateItem { get; set; }

    }
}
