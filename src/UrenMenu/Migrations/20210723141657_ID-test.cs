﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HelpdeskAboTool.Migrations
{
    public partial class IDtest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EstimateEstimateItem",
                table: "EstimateEstimateItem");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "EstimateEstimateItem",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:Identity", "1, 1");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstimateEstimateItem",
                table: "EstimateEstimateItem",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_EstimateEstimateItem_BelongsToEstimatesId",
                table: "EstimateEstimateItem",
                column: "BelongsToEstimatesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_EstimateEstimateItem",
                table: "EstimateEstimateItem");

            migrationBuilder.DropIndex(
                name: "IX_EstimateEstimateItem_BelongsToEstimatesId",
                table: "EstimateEstimateItem");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "EstimateEstimateItem");

            migrationBuilder.AddPrimaryKey(
                name: "PK_EstimateEstimateItem",
                table: "EstimateEstimateItem",
                columns: new[] { "BelongsToEstimatesId", "EstimateItemsListId" });
        }
    }
}
